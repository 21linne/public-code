function turnAround() {
  turnLeft();
  turnLeft();
}
function forwardTwice() {
  moveForward();
  moveForward();
}
function reCenter() {
  turnAround();
  penUp();
  forwardTwice();
  penDown();
}
function drawLine() {
  moveForward();
  turnAround();
  penUp();
  forwardTwice();
  penDown();
  turnAround();
  moveForward();
}
function drawCross() {
  drawLine();
  turnLeft();
  drawLine();  
  turnAround();
  turnLeft();
}
function nextCross() {
  forwardTwice();
  drawCross();
  reCenter();
}
drawCross();
nextCross();
turnLeft();
nextCross();
turnLeft();
nextCross();
turnLeft();
nextCross();
turnLeft();